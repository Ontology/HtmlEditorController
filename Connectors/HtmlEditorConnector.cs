﻿using HtmlEditorController.Services;
using OntologyAppDBConnector;
using OntologyAppDBConnector.Base;
using OntologyClasses.BaseClasses;
using OntoMsg_Module;
using OntoMsg_Module.WebSocketServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using TypedTaggingModule.Connectors;
using TypedTaggingModule.Models;

namespace HtmlEditorController.Connectors
{
    public class HtmlEditorConnector : NotifyPropertyChange
    {
        private clsLocalConfig localConfig;
        public clsLocalConfig LocalConfig
        {
            get { return localConfig; }
            set
            {
                localConfig = value;
            }
        }
        private HtmlEditAgent_Elastic serviceElastic;
        private TypedTaggingConnector typedTaggingConnector;

        public List<clsOntologyItem> AllowedItems { get; private set; }


        public HtmlEditorConnector()
        {
            // local configuration
            localConfig = (clsLocalConfig)LocalConfigManager.GetLocalConfig(((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(true).FirstOrDefault(objAttribute => objAttribute is GuidAttribute)).Value);
            if (localConfig == null)
            {
                localConfig = new clsLocalConfig(new Globals());
                LocalConfigManager.AddLocalConfig(localConfig);
            }

            Initialize();
        }

        public async Task<ResultGetHtmlDocuments> GetHtmlDocumentsByRefs(List<clsOntologyItem> refs)
        {
            var result = serviceElastic.GetHtmlDocuments(refs);
            result.Wait();
            return result.Result;
        }

        public async Task<ConnectorResultHtmlDocument> GetHtmlDocument(string idRef)
        {
            var oItemRef = serviceElastic.GetOItem(idRef, localConfig.Globals.Type_Object);

            var result = new ConnectorResultHtmlDocument
            {
                Result = localConfig.Globals.LState_Success.Clone()
            };
            

            if (oItemRef == null)
            {
                result.Result = localConfig.Globals.LState_Error.Clone();
                return result;
            }

            var resultTask = serviceElastic.GetHtmlDocument(oItemRef);
            resultTask.Wait();
            result.Result = resultTask.Result;
            result.OAItemHtmlDocument = serviceElastic.HtmlCodeAttribute;

            return result;
        }

        public async Task<ResultGetHtmlHierarchy> GetHtmlDocumentWithoutCode(string idRef)
        {
            var result = new ResultGetHtmlHierarchy
            {
                Result = localConfig.Globals.LState_Success.Clone()
            };
            var refItem = serviceElastic.GetOItem(idRef, localConfig.Globals.Type_Object);
            if (refItem == null || refItem.GUID == localConfig.Globals.LState_Error.GUID)
            {
                result.Result = localConfig.Globals.LState_Error.Clone();
                return result;
            }


            var htmlResultTask = serviceElastic.GetRelatedHtmlDocumentWithoutCode(refItem);
            htmlResultTask.Wait();

            if (htmlResultTask.Result == null)
            {
                result.Result = localConfig.Globals.LState_Nothing.Clone();
                return result;
            }

            result.HtmlDocumentOfRef = htmlResultTask.Result;
            return result;
        }

        public async Task<ResultGetHtmlHierarchy> GetHtmlHierarchy(string idRef)
        {
            var result = new ResultGetHtmlHierarchy
            {
                Result = localConfig.Globals.LState_Success.Clone()
            };
            var refItem = serviceElastic.GetOItem(idRef, localConfig.Globals.Type_Object);
            if (refItem == null || refItem.GUID == localConfig.Globals.LState_Error.GUID)
            {
                result.Result = localConfig.Globals.LState_Error.Clone();
                return result;
            }

            var htmlResultTask = serviceElastic.GetRelatedHtmlDocumentWithoutCode(refItem);
            htmlResultTask.Wait();
            
            if (htmlResultTask.Result == null)
            {
                result.Result = localConfig.Globals.LState_Nothing.Clone();
                return result;
            }
            
            var resultTask = GetHierarchy(htmlResultTask.Result.GUID, htmlResultTask.Result.Name);
            result = resultTask.Result;
            result.HtmlDocumentOfRef = htmlResultTask.Result;
            return result;
        }

        public async Task<ResultGetHtmlHierarchy>  GetHierarchy(string idParent, string nameParent)
        {
            var result = new ResultGetHtmlHierarchy
            {
                Result = localConfig.Globals.LState_Success.Clone()
            };
            var tagsTask = GetTags(idParent);
            tagsTask.Wait();

            if (tagsTask.Result.Result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                result.Result = localConfig.Globals.LState_Error.Clone();
                return result;
            }

            var htmlDocuments = new List<clsObjectRel>();

            foreach (var typedTag in tagsTask.Result.TypedTags.Where(typedTag => typedTag.IdTagParent == localConfig.OItem_class_html_document.GUID))
            {
                htmlDocuments.Add(new clsObjectRel
                {
                    ID_Object = idParent,
                    Name_Object = nameParent,
                    ID_Parent_Object = localConfig.OItem_class_html_document.GUID,
                    OrderID = typedTag.OrderId,
                    ID_Other = typedTag.IdTag,
                    Name_Other = typedTag.NameTag,
                    ID_Parent_Other = localConfig.OItem_class_html_document.GUID
                });

                var resultSubTask = GetHierarchy(typedTag.IdTag, typedTag.NameTag);
                resultSubTask.Wait();

                if (resultSubTask.Result.Result.GUID == localConfig.Globals.LState_Error.GUID)
                {
                    return resultSubTask.Result;
                }

                htmlDocuments.AddRange(resultSubTask.Result.Hierarchy);

            }


            result.Hierarchy = htmlDocuments;
            return result;

        }

        public async Task<ConnectorResultTags> GetTags(string idHtmlDocument)
        {
            var oItemHtmlDoc = serviceElastic.GetOItem(idHtmlDocument, localConfig.Globals.Type_Object);

            var result = new ConnectorResultTags
            {
                Result = localConfig.Globals.LState_Success.Clone()
            };


            if (oItemHtmlDoc == null)
            {
                result.Result = localConfig.Globals.LState_Error.Clone();
                return result;
            }

            var tagsTask = typedTaggingConnector.GetTags(oItemHtmlDoc);
            tagsTask.Wait();

            result.Result = tagsTask.Result.Result;
            result.TypedTags = tagsTask.Result.TypedTags;

            return result;
        }


        public HtmlEditorConnector(Globals globals)
        {
            // local configuration
            localConfig = (clsLocalConfig)LocalConfigManager.GetLocalConfig(((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(true).FirstOrDefault(objAttribute => objAttribute is GuidAttribute)).Value);
            if (localConfig == null)
            {
                localConfig = new clsLocalConfig(globals);
                LocalConfigManager.AddLocalConfig(localConfig);
            }

            Initialize();
        }

        private void Initialize()
        {
            serviceElastic = new HtmlEditAgent_Elastic(localConfig);
            typedTaggingConnector = new TypedTaggingConnector(localConfig.Globals);
            AllowedItems = WebSocketBase.GetAnonymousItems(localConfig.Globals);
        }
    }

    public class ConnectorResultHtmlDocument
    {
        public clsOntologyItem Result { get; set; }
        public clsObjectAtt OAItemHtmlDocument { get; set; }
    }

    public class ConnectorResultTags
    {
        public clsOntologyItem Result { get; set; }
        public List<TypedTag> TypedTags { get; set; }
    }

    public class ResultGetHtmlHierarchy
    {
        public clsOntologyItem Result { get; set; }
        public clsOntologyItem HtmlDocumentOfRef { get; set; }
        public List<clsObjectRel> Hierarchy { get; set; }
    }
}
