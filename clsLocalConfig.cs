﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OntologyAppDBConnector;
using ImportExport_Module;
using OntologyClasses.BaseClasses;
using System.Reflection;
using System.Runtime.InteropServices;
using OntologyClasses.Interfaces;

namespace HtmlEditorController
{
    public class clsLocalConfig : ILocalConfig
{
    private const string cstrID_Ontology = "ecd3885771e74128b1616a83d9c275c5";
    private ImportWorker objImport;

    public Globals Globals { get; set; }

    private clsOntologyItem objOItem_DevConfig = new clsOntologyItem();
    public clsOntologyItem OItem_BaseConfig { get; set; }

    private OntologyModDBConnector objDBLevel_Config1;
    private OntologyModDBConnector objDBLevel_Config2;

        // Attributes
    public clsOntologyItem OItem_attributetype_html { get; set; }
        public clsOntologyItem OItem_object_htmleditviewcontroller { get; set; }
        public clsOntologyItem OItem_attributetype_is_active { get; set; }

        // RelationTypes
        public clsOntologyItem OItem_relationtype_reference_to { get; set; }
        public clsOntologyItem OItem_relationtype_uses { get; set; }
        public clsOntologyItem OItem_relationtype_located_at { get; set; }
        public clsOntologyItem OItem_relationtype_is { get; set; }
        public clsOntologyItem OItem_relationtype_contains { get; set; }
        public clsOntologyItem OItem_relationtype_belongs_to { get; set; }

        // Objects
        public clsOntologyItem OItem_object_save { get; set; }
        public clsOntologyItem OItem_object_htmleditcontroller { get; set; }
        public clsOntologyItem OItem_object_html_editor { get; set; }
        public clsOntologyItem OItem_object_d__gitlab_ontologywebcontrollers_htmleditorcontroller_bin_debug_htmleditorcontroller_dll { get; set; }
        public clsOntologyItem OItem_object_baseconfig { get; set; }

        // Classes
        public clsOntologyItem OItem_class_html_document { get; set; }
    public clsOntologyItem OItem_object_tinymceeditor { get; set; }
    public clsOntologyItem OItem_object_htmleditor { get; set; }
    public clsOntologyItem OItem_object_open { get; set; }
    public clsOntologyItem OItem_object_new_file { get; set; }



    private void get_Data_DevelopmentConfig()
    {
        var objORL_Ontology_To_OntolgyItems = new List<clsObjectRel> {new clsObjectRel {ID_Object = cstrID_Ontology,
                                                                                             ID_RelationType = Globals.RelationType_contains.GUID,
                                                                                             ID_Parent_Other = Globals.Class_OntologyItems.GUID}};

        var objOItem_Result = objDBLevel_Config1.GetDataObjectRel(objORL_Ontology_To_OntolgyItems, doIds: false);
        if (objOItem_Result.GUID == Globals.LState_Success.GUID)
        {
            if (objDBLevel_Config1.ObjectRels.Any())
            {

                objORL_Ontology_To_OntolgyItems = objDBLevel_Config1.ObjectRels.Select(oi => new clsObjectRel
                {
                    ID_Object = oi.ID_Other,
                    ID_RelationType = Globals.RelationType_belongingAttribute.GUID
                }).ToList();

                objORL_Ontology_To_OntolgyItems.AddRange(objDBLevel_Config1.ObjectRels.Select(oi => new clsObjectRel
                {
                    ID_Object = oi.ID_Other,
                    ID_RelationType = Globals.RelationType_belongingClass.GUID
                }));
                objORL_Ontology_To_OntolgyItems.AddRange(objDBLevel_Config1.ObjectRels.Select(oi => new clsObjectRel
                {
                    ID_Object = oi.ID_Other,
                    ID_RelationType = Globals.RelationType_belongingObject.GUID
                }));
                objORL_Ontology_To_OntolgyItems.AddRange(objDBLevel_Config1.ObjectRels.Select(oi => new clsObjectRel
                {
                    ID_Object = oi.ID_Other,
                    ID_RelationType = Globals.RelationType_belongingRelationType.GUID
                }));

                objOItem_Result = objDBLevel_Config2.GetDataObjectRel(objORL_Ontology_To_OntolgyItems, doIds: false);
                if (objOItem_Result.GUID == Globals.LState_Success.GUID)
                {
                    if (!objDBLevel_Config2.ObjectRels.Any())
                    {
                        throw new Exception("Config-Error");
                    }
                }
                else
                {
                    throw new Exception("Config-Error");
                }
            }
            else
            {
                throw new Exception("Config-Error");
            }

        }

    }

    public clsLocalConfig()
    {
        Globals = new Globals();
        set_DBConnection();
        get_Config();
    }

    public clsLocalConfig(Globals Globals)
    {
        this.Globals = Globals;
        set_DBConnection();
        get_Config();
    }

    private void set_DBConnection()
    {
        objDBLevel_Config1 = new OntologyModDBConnector(Globals);
        objDBLevel_Config2 = new OntologyModDBConnector(Globals);
        objImport = new ImportWorker(Globals);
    }

    private void get_Config()
    {
        try
        {
            get_Data_DevelopmentConfig();
            get_Config_AttributeTypes();
            get_Config_RelationTypes();
            get_Config_Classes();
            get_Config_Objects();
        }
        catch (Exception ex)
        {
            var objAssembly = Assembly.GetExecutingAssembly();
            AssemblyTitleAttribute[] objCustomAttributes = (AssemblyTitleAttribute[])objAssembly.GetCustomAttributes(typeof(AssemblyTitleAttribute), false);
            var strTitle = "Unbekannt";
            if (objCustomAttributes.Length == 1)
            {
                strTitle = objCustomAttributes.First().Title;
            }
           
            var objOItem_Result = objImport.ImportTemplates(objAssembly);
            if (objOItem_Result.GUID != Globals.LState_Error.GUID)
            {
                get_Data_DevelopmentConfig();
                get_Config_AttributeTypes();
                get_Config_RelationTypes();
                get_Config_Classes();
                get_Config_Objects();
            }
            else
            {
                throw new Exception("Config not importable");
            }
           
        }
    }

    private void get_Config_AttributeTypes()
    {
            var objOList_attributetype_is_active = (from objOItem in objDBLevel_Config1.ObjectRels
                                                    where objOItem.ID_Object == cstrID_Ontology
                                                    join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                    where objRef.Name_Object.ToLower() == "attributetype_is_active".ToLower() && objRef.Ontology == Globals.Type_AttributeType
                                                    select objRef).ToList();

            if (objOList_attributetype_is_active.Any())
            {
                OItem_attributetype_is_active = new clsOntologyItem()
                {
                    GUID = objOList_attributetype_is_active.First().ID_Other,
                    Name = objOList_attributetype_is_active.First().Name_Other,
                    GUID_Parent = objOList_attributetype_is_active.First().ID_Parent_Other,
                    Type = Globals.Type_AttributeType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_attributetype_html = (from objOItem in objDBLevel_Config1.ObjectRels
                                           where objOItem.ID_Object == cstrID_Ontology
                                           join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                           where objRef.Name_Object.ToLower() == "attributetype_html".ToLower() && objRef.Ontology == Globals.Type_AttributeType
                                           select objRef).ToList();

        if (objOList_attributetype_html.Any())
        {
            OItem_attributetype_html = new clsOntologyItem()
            {
                GUID = objOList_attributetype_html.First().ID_Other,
                Name = objOList_attributetype_html.First().Name_Other,
                GUID_Parent = objOList_attributetype_html.First().ID_Parent_Other,
                Type = Globals.Type_AttributeType
            };
        }
        else
        {
            throw new Exception("config err");
        }


    }

    private void get_Config_RelationTypes()
    {
            var objOList_relationtype_uses = (from objOItem in objDBLevel_Config1.ObjectRels
                                              where objOItem.ID_Object == cstrID_Ontology
                                              join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                              where objRef.Name_Object.ToLower() == "relationtype_uses".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                              select objRef).ToList();

            if (objOList_relationtype_uses.Any())
            {
                OItem_relationtype_uses = new clsOntologyItem()
                {
                    GUID = objOList_relationtype_uses.First().ID_Other,
                    Name = objOList_relationtype_uses.First().Name_Other,
                    GUID_Parent = objOList_relationtype_uses.First().ID_Parent_Other,
                    Type = Globals.Type_RelationType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_relationtype_located_at = (from objOItem in objDBLevel_Config1.ObjectRels
                                                    where objOItem.ID_Object == cstrID_Ontology
                                                    join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                    where objRef.Name_Object.ToLower() == "relationtype_located_at".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                    select objRef).ToList();

            if (objOList_relationtype_located_at.Any())
            {
                OItem_relationtype_located_at = new clsOntologyItem()
                {
                    GUID = objOList_relationtype_located_at.First().ID_Other,
                    Name = objOList_relationtype_located_at.First().Name_Other,
                    GUID_Parent = objOList_relationtype_located_at.First().ID_Parent_Other,
                    Type = Globals.Type_RelationType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_relationtype_is = (from objOItem in objDBLevel_Config1.ObjectRels
                                            where objOItem.ID_Object == cstrID_Ontology
                                            join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                            where objRef.Name_Object.ToLower() == "relationtype_is".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                            select objRef).ToList();

            if (objOList_relationtype_is.Any())
            {
                OItem_relationtype_is = new clsOntologyItem()
                {
                    GUID = objOList_relationtype_is.First().ID_Other,
                    Name = objOList_relationtype_is.First().Name_Other,
                    GUID_Parent = objOList_relationtype_is.First().ID_Parent_Other,
                    Type = Globals.Type_RelationType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_relationtype_contains = (from objOItem in objDBLevel_Config1.ObjectRels
                                                  where objOItem.ID_Object == cstrID_Ontology
                                                  join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                  where objRef.Name_Object.ToLower() == "relationtype_contains".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                  select objRef).ToList();

            if (objOList_relationtype_contains.Any())
            {
                OItem_relationtype_contains = new clsOntologyItem()
                {
                    GUID = objOList_relationtype_contains.First().ID_Other,
                    Name = objOList_relationtype_contains.First().Name_Other,
                    GUID_Parent = objOList_relationtype_contains.First().ID_Parent_Other,
                    Type = Globals.Type_RelationType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_relationtype_belongs_to = (from objOItem in objDBLevel_Config1.ObjectRels
                                                    where objOItem.ID_Object == cstrID_Ontology
                                                    join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                    where objRef.Name_Object.ToLower() == "relationtype_belongs_to".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                    select objRef).ToList();

            if (objOList_relationtype_belongs_to.Any())
            {
                OItem_relationtype_belongs_to = new clsOntologyItem()
                {
                    GUID = objOList_relationtype_belongs_to.First().ID_Other,
                    Name = objOList_relationtype_belongs_to.First().Name_Other,
                    GUID_Parent = objOList_relationtype_belongs_to.First().ID_Parent_Other,
                    Type = Globals.Type_RelationType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_relationtype_reference_to = (from objOItem in objDBLevel_Config1.ObjectRels
                                                  where objOItem.ID_Object == cstrID_Ontology
                                                  join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                  where objRef.Name_Object.ToLower() == "relationtype_reference_to".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                  select objRef).ToList();

        if (objOList_relationtype_reference_to.Any())
        {
            OItem_relationtype_reference_to = new clsOntologyItem()
            {
                GUID = objOList_relationtype_reference_to.First().ID_Other,
                Name = objOList_relationtype_reference_to.First().Name_Other,
                GUID_Parent = objOList_relationtype_reference_to.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }


    }

    private void get_Config_Objects()
    {
            var objOList_object_htmleditviewcontroller = (from objOItem in objDBLevel_Config1.ObjectRels
                                                          where objOItem.ID_Object == cstrID_Ontology
                                                          join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                          where objRef.Name_Object.ToLower() == "object_htmleditviewcontroller".ToLower() && objRef.Ontology == Globals.Type_Object
                                                          select objRef).ToList();

            if (objOList_object_htmleditviewcontroller.Any())
            {
                OItem_object_htmleditviewcontroller = new clsOntologyItem()
                {
                    GUID = objOList_object_htmleditviewcontroller.First().ID_Other,
                    Name = objOList_object_htmleditviewcontroller.First().Name_Other,
                    GUID_Parent = objOList_object_htmleditviewcontroller.First().ID_Parent_Other,
                    Type = Globals.Type_Object
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_object_html_editor = (from objOItem in objDBLevel_Config1.ObjectRels
                                               where objOItem.ID_Object == cstrID_Ontology
                                               join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                               where objRef.Name_Object.ToLower() == "object_html_editor".ToLower() && objRef.Ontology == Globals.Type_Object
                                               select objRef).ToList();

            if (objOList_object_html_editor.Any())
            {
                OItem_object_html_editor = new clsOntologyItem()
                {
                    GUID = objOList_object_html_editor.First().ID_Other,
                    Name = objOList_object_html_editor.First().Name_Other,
                    GUID_Parent = objOList_object_html_editor.First().ID_Parent_Other,
                    Type = Globals.Type_Object
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_object_d__gitlab_ontologywebcontrollers_htmleditorcontroller_bin_debug_htmleditorcontroller_dll = (from objOItem in objDBLevel_Config1.ObjectRels
                                                                                                                            where objOItem.ID_Object == cstrID_Ontology
                                                                                                                            join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                                                                                            where objRef.Name_Object.ToLower() == "object_d__gitlab_ontologywebcontrollers_htmleditorcontroller_bin_debug_htmleditorcontroller_dll".ToLower() && objRef.Ontology == Globals.Type_Object
                                                                                                                            select objRef).ToList();

            if (objOList_object_d__gitlab_ontologywebcontrollers_htmleditorcontroller_bin_debug_htmleditorcontroller_dll.Any())
            {
                OItem_object_d__gitlab_ontologywebcontrollers_htmleditorcontroller_bin_debug_htmleditorcontroller_dll = new clsOntologyItem()
                {
                    GUID = objOList_object_d__gitlab_ontologywebcontrollers_htmleditorcontroller_bin_debug_htmleditorcontroller_dll.First().ID_Other,
                    Name = objOList_object_d__gitlab_ontologywebcontrollers_htmleditorcontroller_bin_debug_htmleditorcontroller_dll.First().Name_Other,
                    GUID_Parent = objOList_object_d__gitlab_ontologywebcontrollers_htmleditorcontroller_bin_debug_htmleditorcontroller_dll.First().ID_Parent_Other,
                    Type = Globals.Type_Object
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_object_baseconfig = (from objOItem in objDBLevel_Config1.ObjectRels
                                              where objOItem.ID_Object == cstrID_Ontology
                                              join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                              where objRef.Name_Object.ToLower() == "object_baseconfig".ToLower() && objRef.Ontology == Globals.Type_Object
                                              select objRef).ToList();

            if (objOList_object_baseconfig.Any())
            {
                OItem_object_baseconfig = new clsOntologyItem()
                {
                    GUID = objOList_object_baseconfig.First().ID_Other,
                    Name = objOList_object_baseconfig.First().Name_Other,
                    GUID_Parent = objOList_object_baseconfig.First().ID_Parent_Other,
                    Type = Globals.Type_Object
                };
            }
            else
            {
                throw new Exception("config err");
            }
            var objOList_object_save = (from objOItem in objDBLevel_Config1.ObjectRels
                                    where objOItem.ID_Object == cstrID_Ontology
                                    join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                    where objRef.Name_Object.ToLower() == "object_save".ToLower() && objRef.Ontology == Globals.Type_Object
                                    select objRef).ToList();

        if (objOList_object_save.Any())
        {
            OItem_object_save = new clsOntologyItem()
            {
                GUID = objOList_object_save.First().ID_Other,
                Name = objOList_object_save.First().Name_Other,
                GUID_Parent = objOList_object_save.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_object_tinymceeditor = (from objOItem in objDBLevel_Config1.ObjectRels
                                             where objOItem.ID_Object == cstrID_Ontology
                                             join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                             where objRef.Name_Object.ToLower() == "object_tinymceeditor".ToLower() && objRef.Ontology == Globals.Type_Object
                                             select objRef).ToList();

        if (objOList_object_tinymceeditor.Any())
        {
            OItem_object_tinymceeditor = new clsOntologyItem()
            {
                GUID = objOList_object_tinymceeditor.First().ID_Other,
                Name = objOList_object_tinymceeditor.First().Name_Other,
                GUID_Parent = objOList_object_tinymceeditor.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_object_htmleditor = (from objOItem in objDBLevel_Config1.ObjectRels
                                          where objOItem.ID_Object == cstrID_Ontology
                                          join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                          where objRef.Name_Object.ToLower() == "object_htmleditor".ToLower() && objRef.Ontology == Globals.Type_Object
                                          select objRef).ToList();

        if (objOList_object_htmleditor.Any())
        {
            OItem_object_htmleditor = new clsOntologyItem()
            {
                GUID = objOList_object_htmleditor.First().ID_Other,
                Name = objOList_object_htmleditor.First().Name_Other,
                GUID_Parent = objOList_object_htmleditor.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_object_open = (from objOItem in objDBLevel_Config1.ObjectRels
                                    where objOItem.ID_Object == cstrID_Ontology
                                    join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                    where objRef.Name_Object.ToLower() == "object_open".ToLower() && objRef.Ontology == Globals.Type_Object
                                    select objRef).ToList();

        if (objOList_object_open.Any())
        {
            OItem_object_open = new clsOntologyItem()
            {
                GUID = objOList_object_open.First().ID_Other,
                Name = objOList_object_open.First().Name_Other,
                GUID_Parent = objOList_object_open.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_object_new_file = (from objOItem in objDBLevel_Config1.ObjectRels
                                        where objOItem.ID_Object == cstrID_Ontology
                                        join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                        where objRef.Name_Object.ToLower() == "object_new_file".ToLower() && objRef.Ontology == Globals.Type_Object
                                        select objRef).ToList();

        if (objOList_object_new_file.Any())
        {
            OItem_object_new_file = new clsOntologyItem()
            {
                GUID = objOList_object_new_file.First().ID_Other,
                Name = objOList_object_new_file.First().Name_Other,
                GUID_Parent = objOList_object_new_file.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }


    }

    private void get_Config_Classes()
    {
        var objOList_class_html_document = (from objOItem in objDBLevel_Config1.ObjectRels
                                            where objOItem.ID_Object == cstrID_Ontology
                                            join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                            where objRef.Name_Object.ToLower() == "class_html_document".ToLower() && objRef.Ontology == Globals.Type_Class
                                            select objRef).ToList();

        if (objOList_class_html_document.Any())
        {
            OItem_class_html_document = new clsOntologyItem()
            {
                GUID = objOList_class_html_document.First().ID_Other,
                Name = objOList_class_html_document.First().Name_Other,
                GUID_Parent = objOList_class_html_document.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }


    }

        public string IdLocalConfig
        {
            get
            {
                var attrib =
                      Assembly.GetExecutingAssembly()
                          .GetCustomAttributes(true)
                          .FirstOrDefault(objAttribute => objAttribute is GuidAttribute);
                if (attrib != null)
                {
                    return ((GuidAttribute)attrib).Value;
                }
                else
                {
                    return null;
                }
            }
        }
    }

}