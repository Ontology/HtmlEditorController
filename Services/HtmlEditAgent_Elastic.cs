﻿using ElasticSearchNestConnector;
using HtmlEditorController.Notifications;
using OntologyAppDBConnector;
using OntologyAppDBConnector.Base;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TypedTaggingModule.Models;

namespace HtmlEditorController.Services
{
    public class HtmlEditAgent_Elastic : NotifyPropertyChange
    {
        private clsLocalConfig localConfig;

        private clsOntologyItem oItemRef;
        public clsOntologyItem OItemRef
        {
            get { return oItemRef; }
            set
            {
                oItemRef = value;
            }
        }
        private clsOntologyItem oItemHtmlDocument;
        public clsOntologyItem OItemHtmlDocument
        {
            get { return oItemHtmlDocument; }
            set
            {
                oItemHtmlDocument = value;
            }
        }

        private bool leftRightHtmlDocRef;


        private OntologyModDBConnector dbReader_HtmlDoc;
        private OntologyModDBConnector dbReader_HtmlCode;

        private Thread getHtmlDocumentAsync;

        private clsRelationConfig relationConfig;
        private clsTransaction transaction;

        private clsObjectAtt htmlCodeAttribute;
        public clsObjectAtt HtmlCodeAttribute
        {
            get { return htmlCodeAttribute; }
            set
            {
                htmlCodeAttribute = value;
            }
        }

        private clsOntologyItem resultData;
        public clsOntologyItem ResultData
        {
            get { return resultData; }
            set
            {
                resultData = value;
                RaisePropertyChanged(NotifyChanges.Elastic_ResultData);
            }
        }

        public async Task<clsOntologyItem> GetHtmlDocument(clsOntologyItem oItemRef)
        {
            this.oItemRef = oItemRef;
            GetRelatedHtmlDocumentAsync();
            return ResultData;

        } 

        public async Task<ResultGetHtmlDocuments> GetHtmlDocuments(List<clsOntologyItem> refItems)
        {
            var result =
                new ResultGetHtmlDocuments
                {
                    Result = localConfig.Globals.LState_Success.Clone()
                };

            var search = refItems.Select(refItem => new clsObjectRel
            {
                ID_Other = refItem.GUID,
                ID_Parent_Object = localConfig.OItem_class_html_document.GUID,
                ID_RelationType = localConfig.OItem_relationtype_reference_to.GUID
            }).ToList();

            var dbReader = new OntologyModDBConnector(localConfig.Globals);

            if (search.Any())
            {
                result.Result = dbReader.GetDataObjectRel(search);
            }
            

            if (result.Result.GUID == localConfig.Globals.LState_Success.GUID)
            {
                result.HtmlDocumentsToRef = dbReader.ObjectRels;
            }

            return result;
        }

        public clsOntologyItem GetRelatedHtmlDocument(clsOntologyItem oItemRef)
        {
            this.oItemRef = oItemRef;
            if (getHtmlDocumentAsync != null)
            {
                try
                {
                    getHtmlDocumentAsync.Abort();
                }
                catch(Exception ex)
                {
                    
                }

                

            }

            getHtmlDocumentAsync = new Thread(GetRelatedHtmlDocumentAsync);

            getHtmlDocumentAsync.Start();

            return localConfig.Globals.LState_Success.Clone();
        }

        private void GetRelatedHtmlDocumentAsync()
        {
            var result = localConfig.Globals.LState_Success.Clone();
            if (oItemRef.GUID_Parent != localConfig.OItem_class_html_document.GUID)
            {
                result = GetSubData_001_HtmlDocument();
                if (leftRightHtmlDocRef)
                {
                    oItemHtmlDocument = dbReader_HtmlDoc.ObjectRels.Select(objRel => new clsOntologyItem
                    {
                        GUID = objRel.ID_Other,
                        Name = objRel.Name_Other,
                        GUID_Parent = objRel.ID_Parent_Other,
                        Type = objRel.Ontology
                    }).FirstOrDefault();


                }
                else
                {
                    oItemHtmlDocument = dbReader_HtmlDoc.ObjectRels.Select(objRel => new clsOntologyItem
                    {
                        GUID = objRel.ID_Object,
                        Name = objRel.Name_Object,
                        GUID_Parent = objRel.ID_Parent_Object,
                        Type = localConfig.Globals.Type_Object
                    }).FirstOrDefault();
                }
            }
            else
            {
                oItemHtmlDocument = oItemRef;
            }

            if (oItemHtmlDocument != null)
            {
                result = GetSubData_002_HTMLCode();
            }
            else
            {
                dbReader_HtmlCode.ObjAtts.Clear();
            }

            if (result.GUID == localConfig.Globals.LState_Success.GUID)
            {
                HtmlCodeAttribute = dbReader_HtmlCode.ObjAtts.FirstOrDefault();
            }

            ResultData = result;
        }

        public async Task<clsOntologyItem> GetRelatedHtmlDocumentWithoutCode(clsOntologyItem oItemRef)
        {
            this.OItemRef = oItemRef;
            var result = localConfig.Globals.LState_Success.Clone();
            if (oItemRef.GUID_Parent != localConfig.OItem_class_html_document.GUID)
            {
                result = GetSubData_001_HtmlDocument();
                if (leftRightHtmlDocRef)
                {
                    oItemHtmlDocument = dbReader_HtmlDoc.ObjectRels.Select(objRel => new clsOntologyItem
                    {
                        GUID = objRel.ID_Other,
                        Name = objRel.Name_Other,
                        GUID_Parent = objRel.ID_Parent_Other,
                        Type = objRel.Ontology
                    }).FirstOrDefault();


                }
                else
                {
                    oItemHtmlDocument = dbReader_HtmlDoc.ObjectRels.Select(objRel => new clsOntologyItem
                    {
                        GUID = objRel.ID_Object,
                        Name = objRel.Name_Object,
                        GUID_Parent = objRel.ID_Parent_Object,
                        Type = localConfig.Globals.Type_Object
                    }).FirstOrDefault();
                }
            }
            else
            {
                oItemHtmlDocument = oItemRef;
            }

            return oItemHtmlDocument;
        }

        private clsOntologyItem GetSubData_001_HtmlDocument()
        {
            var searchHtmlDocument = new List<clsObjectRel>
            {
                new clsObjectRel
                {
                    ID_Object = oItemRef.GUID,
                    ID_Parent_Other = localConfig.OItem_class_html_document.GUID
                }
            };

            var result = dbReader_HtmlDoc.GetDataObjectRel(searchHtmlDocument);

            if (result.GUID == localConfig.Globals.LState_Error.GUID) return result;
            
            if (dbReader_HtmlDoc.ObjectRels.Any())
            {

                leftRightHtmlDocRef = true;
                return result;
            }

            searchHtmlDocument = new List<clsObjectRel>
            {
                new clsObjectRel
                {
                    ID_Other = oItemRef.GUID,
                    ID_Parent_Object = localConfig.OItem_class_html_document.GUID
                }
            };

            result = dbReader_HtmlDoc.GetDataObjectRel(searchHtmlDocument);
            leftRightHtmlDocRef = false;
            return result;
        }


        private clsOntologyItem GetSubData_002_HTMLCode()
        {
            var searchHtmlCode = new List<clsObjectAtt> { new clsObjectAtt
                {
                    ID_Object = oItemHtmlDocument.GUID,
                    ID_AttributeType = localConfig.OItem_attributetype_html.GUID
                }
            };

            var result = dbReader_HtmlCode.GetDataObjectAtt(searchHtmlCode);

            return result;
        }

        public clsOntologyItem SaveDocName(string name)
        {
            var result = localConfig.Globals.LState_Success.Clone();

            transaction.ClearItems();

            if (OItemHtmlDocument != null)
            {
                OItemHtmlDocument.Name = name;
                result = transaction.do_Transaction(OItemHtmlDocument);
            }
            else
            {
                oItemHtmlDocument = new clsOntologyItem
                {
                    GUID = localConfig.Globals.NewGUID,
                    Name = oItemRef.Name,
                    GUID_Parent = localConfig.OItem_class_html_document.GUID,
                    Type = localConfig.Globals.Type_Object
                };

                result = transaction.do_Transaction(oItemHtmlDocument);

                if (result.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    var refRelHtmlDoc = relationConfig.Rel_ObjectRelation(OItemHtmlDocument, OItemRef, localConfig.OItem_relationtype_reference_to);
                    result = transaction.do_Transaction(refRelHtmlDoc, true);
                    if (result.GUID == localConfig.Globals.LState_Success.GUID)
                    {
                        result = SaveHtmlContent("");
                    }
                    
                }
            }

            return result;
        }

        public async Task<clsOntologyItem> ArchiveHtmlContent(clsObjectAtt oAttributeHtmlCode)
        {

            var appDBSeletor = new clsUserAppDBSelector(localConfig.Globals.Server,
                localConfig.Globals.Port,
                "HtmlEditorController".ToLower(),
                localConfig.Globals.SearchRange,
                localConfig.Globals.Session);

            var appDBUpdater = new clsUserAppDBUpdater(appDBSeletor);

            var doc = new clsAppDocuments
            {
                Id = localConfig.Globals.NewGUID,
                Dict = new Dictionary<string, object>()
            };

            doc.Dict = new Dictionary<string, object>();
            doc.Dict.Add(nameof(oAttributeHtmlCode.ID_Attribute), oAttributeHtmlCode.ID_Attribute);
            doc.Dict.Add(nameof(oAttributeHtmlCode.ID_Object), oAttributeHtmlCode.ID_Object);
            doc.Dict.Add(nameof(oAttributeHtmlCode.Val_String), oAttributeHtmlCode.Val_String);
            doc.Dict.Add("datestamp", DateTime.Now);

            var result = appDBUpdater.SaveDoc(new List<clsAppDocuments> { doc }, "code");

           return result;
        }

        

        public clsOntologyItem SaveHtmlContent(string html)
        {
            var result = localConfig.Globals.LState_Success.Clone();
            transaction.ClearItems();
            if (oItemHtmlDocument != null)
            {
                var saveAttribute = relationConfig.Rel_ObjectAttribute(oItemHtmlDocument, localConfig.OItem_attributetype_html, html);
                var resultTask = ArchiveHtmlContent(saveAttribute);
                result = transaction.do_Transaction(saveAttribute, true);
                HtmlCodeAttribute = transaction.OItem_Last.OItemObjectAtt;
                return result;
            }
            else
            {
                result = SaveDocName(oItemRef.Name);
                if (result.GUID == localConfig.Globals.LState_Error.GUID) return result;
                
                if (oItemRef.GUID_Parent == localConfig.OItem_class_html_document.GUID)
                {
                    var saveAttribute = relationConfig.Rel_ObjectAttribute(oItemHtmlDocument, localConfig.OItem_attributetype_html, html);
                    result = transaction.do_Transaction(saveAttribute, true);
                    return result;
                }
                else
                {
                    

                    if (result.GUID == localConfig.Globals.LState_Success.GUID)
                    {
                        var saveAttribute = relationConfig.Rel_ObjectAttribute(oItemHtmlDocument, localConfig.OItem_attributetype_html, html);
                        result = transaction.do_Transaction(saveAttribute, true);
                        if (result.GUID == localConfig.Globals.LState_Error.GUID)
                        {
                            transaction.rollback();
                        }

                        
                    }

                    if (result.GUID == localConfig.Globals.LState_Error.GUID)
                    {
                        HtmlCodeAttribute = dbReader_HtmlCode.ObjAtts.FirstOrDefault();
                    }
                    else
                    {
                        HtmlCodeAttribute = transaction.OItem_Last.OItemObjectAtt;
                    }

                    return result;
                }
                
            }
        }

        public clsOntologyItem GetOItem(string id, string type)
        {
            var dbReader = new OntologyModDBConnector(localConfig.Globals);
            return dbReader.GetOItem(id, type);
        }

        public HtmlEditAgent_Elastic(clsLocalConfig localConfig)
        {
            this.localConfig = localConfig;

            Initialize();
        }

        private void Initialize()
        {
            dbReader_HtmlDoc = new OntologyModDBConnector(localConfig.Globals);
            dbReader_HtmlCode = new OntologyModDBConnector(localConfig.Globals);
            relationConfig = new clsRelationConfig(localConfig.Globals);
            transaction = new clsTransaction(localConfig.Globals);
        }
    }

    public class ResultGetHtmlDocuments
    {
        public clsOntologyItem Result { get; set; }
        public List<clsObjectRel> HtmlDocumentsToRef { get; set; }
    }

    public class ResultGetHtmlDocumentWithoutCode
    {
        public clsOntologyItem Result { get; set; }
        public clsOntologyItem HtmlDoc { get; set; }
    }
}
