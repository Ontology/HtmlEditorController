﻿using HtmlEditorController.Services;
using HtmlEditorController.Translations;
using MediaViewerController.Connectors;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Base;
using OntoMsg_Module.Models;
using OntoMsg_Module.Notifications;
using OntoMsg_Module.StateMachines;
using OntoMsg_Module.WebSocketServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Web;
using TypedTaggingModule.Connectors;
using System.ComponentModel;

namespace HtmlEditorController.Controllers
{
    public class HtmlEditViewController : HtmlEditViewModel, IViewController
    {
        private WebsocketServiceAgent webSocketServiceAgent;

        private TranslationController translationController = new TranslationController();

        private HtmlEditAgent_Elastic serviceAgentElastic;
        private TypedTaggingConnector typedTaggingConnector;
        private MediaConnector mediaConnector;
        private HtmlAnaylzer htmlAnalyzer;

        private clsOntologyItem referenceParentItem;
        private clsOntologyItem referenceItem;

        private clsLocalConfig localConfig;

        public IControllerStateMachine StateMachine { get; private set; }
        public ControllerStateMachine LocalStateMachine
        {
            get
            {
                if (StateMachine == null)
                {
                    return null;
                }

                return (ControllerStateMachine)StateMachine;
            }
        }

        public HtmlEditViewController()
        {

            // local configuration
            localConfig = (clsLocalConfig)LocalConfigManager.GetLocalConfig(((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(true).FirstOrDefault(objAttribute => objAttribute is GuidAttribute)).Value);
            if (localConfig == null)
            {
                localConfig = new clsLocalConfig(new Globals());
                LocalConfigManager.AddLocalConfig(localConfig);
            }

            Initialize();
           
            PropertyChanged += AudioPlayerViewController_PropertyChanged;
        }

        private void AudioPlayerViewController_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            var property = ViewModelProperties.FirstOrDefault(viewProperty => viewProperty.Property.Name == e.PropertyName);

            if (property == null) return;

            property.ViewItem.AddValue(property.Property.GetValue(this));

            if (e.PropertyName == Notifications.NotifyChanges.ViewModel_IsSuccessful_Login ||
                e.PropertyName == Notifications.NotifyChanges.ViewModel_IsToggled_NextDataIsHtmlCode ||
                e.PropertyName == Notifications.NotifyChanges.ViewModel_DataText_Name ||
                e.PropertyName == Notifications.NotifyChanges.ViewModel_IsEnabled_HtmlEditor ||
                e.PropertyName == Notifications.NotifyChanges.View_Text_View ||
                e.PropertyName == Notifications.NotifyChanges.ViewModel_IsToggled_Listen ||
                e.PropertyName == Notifications.NotifyChanges.ViewModel_Text_Insert ||
                e.PropertyName == Notifications.NotifyChanges.ViewModel_DataText_Anchor ||
                e.PropertyName == nameof(IsEnabled_ShowViewerArea))
            {
                webSocketServiceAgent.SendPropertyChange(e.PropertyName);
            }
        }

        private void Initialize()
        {

            var stateMachine = new ControllerStateMachine(StateMachineType.BlockSelectingSelected | StateMachineType.BlockLoadingSelected);
            stateMachine.IsControllerListen = true;
            StateMachine = stateMachine;
            stateMachine.PropertyChanged += StateMachine_PropertyChanged;
            stateMachine.loadSelectedItem += StateMachine_loadSelectedItem;
            stateMachine.loginSucceded += StateMachine_loginSucceded;
            stateMachine.openedSocket += StateMachine_openedSocket;
            stateMachine.closedSocket += StateMachine_closedSocket;

            Label_Name = "Name:";
            serviceAgentElastic = new HtmlEditAgent_Elastic(localConfig);
            serviceAgentElastic.PropertyChanged += ServiceAgentElastic_PropertyChanged;

            typedTaggingConnector = new TypedTaggingConnector(localConfig.Globals);
            typedTaggingConnector.PropertyChanged += TypedTaggingConnector_PropertyChanged;

            mediaConnector = new MediaConnector(localConfig.Globals);
            mediaConnector.PropertyChanged += MediaConnector_PropertyChanged;

            htmlAnalyzer = new HtmlAnaylzer(localConfig);
            htmlAnalyzer.PropertyChanged += HtmlAnalyzer_PropertyChanged;

        }

        private void StateMachine_closedSocket()
        {
            webSocketServiceAgent.RemoveAllResources();
            serviceAgentElastic = null;
            translationController = null;
        }

        private void StateMachine_openedSocket()
        {
            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                EndPointId = webSocketServiceAgent.EndpointId,
                EndpointType = EndpointType.Sender,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });

            webSocketServiceAgent.SendModel();
        }

        private void StateMachine_loginSucceded()
        {
            
            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                ChannelTypeId = Channels.ParameterList,
                EndPointId = webSocketServiceAgent.EndpointId,
                EndpointType = EndpointType.Receiver,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });

            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                ChannelTypeId = Channels.AppliedObjects,
                EndPointId = webSocketServiceAgent.EndpointId,
                EndpointType = EndpointType.Receiver,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });

            IsToggled_Listen = true;
            IsToggled_InsertAnchor = false;
            IsToggled_InsertLink = false;

            webSocketServiceAgent.SendCommand("tglNoInsert");

            if (referenceItem != null)
            {
                SelectedItem();
            }

            
            
        }

        private void StateMachine_loadSelectedItem(clsOntologyItem oItemSelected)
        {
        }

        private void StateMachine_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(ControllerStateMachine.LoginSuccessful))
            {
                
                IsEnabled_ShowViewerArea = !LocalStateMachine.LoginSuccessful;
                IsSuccessful_Login = LocalStateMachine.LoginSuccessful;
            }
        }

        private void HtmlAnalyzer_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof( htmlAnalyzer.GuidListResult))
            {
                if (htmlAnalyzer.GuidListResult.Result.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    
                    var resultTask = typedTaggingConnector.GetTags(serviceAgentElastic.OItemHtmlDocument);
                    
                    
                }
                
            }
        }

        private void MediaConnector_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(MediaConnector.ResultMultimediaSessionFile))
            {
                var result = mediaConnector.ResultMultimediaSessionFile;
                if (result.Result.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    if (result.MediaType == MediaType.Image)
                    {
                        var html = "<img src='" + result.SessionFile.FileUri.AbsoluteUri + "' id='_" + result.OMultimediaItem.GUID_Related + "' class='" + Notifications.NotifyChanges.Class_OntologyItem + "'  > ";
                        Text_Insert = html;
                    }
                    else if (result.MediaType == MediaType.PDF)
                    {
                        var html = "<a href='" + result.SessionFile.FileUri.AbsoluteUri + "' id='_" + result.OMultimediaItem.GUID_Related + "' class='" + Notifications.NotifyChanges.Class_OntologyItem + "' >" + result.OMultimediaItem.Name + "</a>";
                        Text_Insert = html;
                    }
                    else if (result.MediaType == MediaType.Audio)
                    {
                        var html = "<audio controls><source src='" + result.SessionFile.FileUri.AbsoluteUri + "' id='_" + result.OMultimediaItem.GUID_Related + "' class='" + Notifications.NotifyChanges.Class_OntologyItem + "'></audio>";
                        Text_Insert = html;
                    }
                    else if (result.MediaType == MediaType.Video)
                    {
                        var html = "<video controls><source src='" + result.SessionFile.FileUri.AbsoluteUri + "' id='_" + result.OMultimediaItem.GUID_Related + "' class='" + Notifications.NotifyChanges.Class_OntologyItem + "'></video>";
                        Text_Insert = html;
                    }
                    
                }
            }
        }

        private void TypedTaggingConnector_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(TypedTaggingConnector.ResultSaveTypedTags))
            {
                if (typedTaggingConnector.ResultSaveTypedTags.Result.GUID== localConfig.Globals.LState_Success.GUID)
                {
                    typedTaggingConnector.ResultSaveTypedTags.TypedTags.ForEach(typedTag =>
                    {
                        ViewMetaItem view = null;

                        var oItem = new clsOntologyItem
                        {
                            GUID = typedTag.IdTag,
                            Name = typedTag.NameTag,
                            GUID_Parent = typedTag.IdTagParent,
                            Type = typedTag.TagType
                        };

                        if (oItem.GUID_Parent == localConfig.OItem_class_html_document.GUID)
                        {
                            view = OntoMsg_Module.ModuleDataExchanger.GetViewById("ea47d9ef62d5497abf96426855ab2622");
                        }
                        else if (oItem.GUID_Parent == mediaConnector.OItemImageClass.GUID ||
                            oItem.GUID_Parent == mediaConnector.OItemMediaItemClass.GUID ||
                            oItem.GUID_Parent == mediaConnector.OItemPDFClass.GUID)
                        {
                            oItem.GUID_Related = typedTag.IdTypedTag;
                            var resultTask = mediaConnector.GetMediaSessionFile(oItem, webSocketServiceAgent);
                        }
                        else
                        {
                            view = OntoMsg_Module.ModuleDataExchanger.GetViewById("6229a154ddd549a29af33e108f2a6104");
                        }

                        if (IsToggled_InsertLink)
                        {
                            var html = "<a class='" + Notifications.NotifyChanges.Class_OntologyItem + "' name='_" + typedTag.IdTag + "' target=\"_blank\" href=\"" + OntoMsg_Module.ModuleDataExchanger.CurrentWebApp.EntryUrl + (OntoMsg_Module.ModuleDataExchanger.CurrentWebApp.EntryUrl.EndsWith("/") ? "" : "/") + view.NameCommandLineRun + "?Class=" + oItem.GUID_Parent + "&Object=" + oItem.GUID + "\">" + HttpUtility.HtmlEncode(oItem.Name) + "</a>";
                            Text_Insert = html;
                        }
                        else if (IsToggled_InsertAnchor)
                        {
                            var id = oItem.GUID;
                            DataText_Anchor = id;
                        }
                    });
                    
                }
            }
            else if (e.PropertyName == nameof(TypedTaggingConnector.ResultSaveTypedTagsNoView))
            {
                var resultTask = typedTaggingConnector.GetTags(serviceAgentElastic.OItemHtmlDocument);  
            }
            else if (e.PropertyName == nameof(TypedTaggingConnector.ResultFoundTypedTags))
            {
                if (typedTaggingConnector.ResultFoundTypedTags.Result.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    var typedTagsToRemove = (from typedTag in typedTaggingConnector.ResultFoundTypedTags.TypedTags
                                             join guid in htmlAnalyzer.GuidListResult.GuidList on typedTag.IdTag equals guid into guids
                                             from guid in guids.DefaultIfEmpty()
                                             where string.IsNullOrEmpty(guid)
                                             select typedTag).Select(typedTag =>
                                             {
                                                 var result = new clsOntologyItem
                                                 {
                                                     GUID = typedTag.IdTypedTag,
                                                     Name = typedTag.NameTypedTag,
                                                     Type = localConfig.Globals.Type_Object
                                                 };
                                                 return result;
                                             }).ToList();


                    var orderId = 1;
                    var itemsToAdd = (from guid in htmlAnalyzer.GuidListResult.GuidList
                                      join typedTag in typedTaggingConnector.ResultFoundTypedTags.TypedTags on guid equals typedTag.IdTag into typedTags
                                      from typedTag in typedTags.DefaultIfEmpty()
                                      where typedTag == null
                                      select guid).Select(guid =>
                                      {
                                          var oItem = serviceAgentElastic.GetOItem(guid, localConfig.Globals.Type_Object);
                                          if (oItem.GUID_Related == localConfig.Globals.LState_Error.GUID)
                                          {
                                              return null;
                                          }
                                          oItem.Val_Long = orderId;
                                          return oItem;
                                      }).Where(itemToAdd => itemToAdd != null).ToList();

                    if (itemsToAdd.Any())
                    {
                        var resultTaskAdd = typedTaggingConnector.SaveTags(serviceAgentElastic.OItemHtmlDocument, itemsToAdd, webSocketServiceAgent.oItemUser, webSocketServiceAgent.oItemGroup, false);
                    }


                    typedTagsToRemove.ForEach(typedTag =>
                    {
                        var resultTask1 = typedTaggingConnector.DeleteTag(typedTag);
                    });

                    if (!itemsToAdd.Any() && !typedTagsToRemove.Any())
                    {

                        var typedTagsToReorder = (from guid in htmlAnalyzer.GuidListResult.GuidList
                                                  join typedTag in typedTaggingConnector.ResultFoundTypedTags.TypedTags on guid equals typedTag.IdTag
                                                  select typedTag).ToList();

                        if (typedTagsToReorder.Any())
                        {
                            var resultTask2 = typedTaggingConnector.ReorderTypedTags(typedTagsToReorder);
                        }
                        
                        
                    }

                }
            }
            else if (e.PropertyName == nameof(TypedTaggingConnector.ResultReorderTypedTag))
            {
                if (typedTaggingConnector.ResultReorderTypedTag.GUID == localConfig.Globals.LState_Success.GUID)
                {

                }
            }

        }

        private void ServiceAgentElastic_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == Notifications.NotifyChanges.Elastic_ResultData)
            {
                try
                {
                    if (serviceAgentElastic.ResultData.GUID == localConfig.Globals.LState_Error.GUID)
                    {
                        return;


                    }
                    if (serviceAgentElastic.ResultData.GUID == localConfig.Globals.LState_Success.GUID)
                    {

                        if (serviceAgentElastic.OItemHtmlDocument == null)
                        {
                            DataText_Name = referenceItem.Name;
                            IsEnabled_HtmlEditor = true;
                        }
                        else
                        {
                            if (IsSuccessful_Login)
                            {
                                var result = htmlAnalyzer.GetGuidList(serviceAgentElastic.HtmlCodeAttribute);
                            }


                            DataText_Name = serviceAgentElastic.OItemHtmlDocument.Name;
                            IsEnabled_HtmlEditor = true;
                        }
                        IsToggled_NextDataIsHtmlCode = true;
                    }
                }
                catch (Exception)
                {
                    IsEnabled_HtmlEditor = false;
                    IsEnabled_ShowViewerArea = false;
                    return;
                }
                
                


            }


        }

        public void InitializeViewController(WebsocketServiceAgent webSocketServiceAgent)
        {
            this.webSocketServiceAgent = webSocketServiceAgent;
            this.webSocketServiceAgent.PropertyChanged += WebSocketServiceAgent_PropertyChanged;
            this.webSocketServiceAgent.comServerOnMessage += WebSocketServiceAgent_comServerOnMessage;
            this.webSocketServiceAgent.comServerOpened += WebSocketServiceAgent_comServerOpened;
            this.webSocketServiceAgent.receivedData += WebSocketServiceAgent_receivedData;

        }

        private void WebSocketServiceAgent_receivedData(byte[] byteBuffer)
        {
            webSocketServiceAgent.InterpretNextAsBytes = false;
            var content = Encoding.UTF8.GetString(byteBuffer);
            var result = serviceAgentElastic.SaveHtmlContent(content);
            
            if (result.GUID == localConfig.Globals.LState_Success.GUID)
            {
                webSocketServiceAgent.SendCommand("content.saved");
            }
            else if (result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                IsToggled_NextDataIsHtmlCode = true;
                
            }
        }

        private void WebSocketServiceAgent_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_DataText_SessionId)
            {


            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_Command_RequestedCommand)
            {
                if (webSocketServiceAgent.Command_RequestedCommand == Notifications.NotifyChanges.Command_SendContent)
                {
                    if (serviceAgentElastic.HtmlCodeAttribute != null)
                    {
                        var htmlCode = serviceAgentElastic.HtmlCodeAttribute.Val_String;
                        webSocketServiceAgent.SendString(string.IsNullOrEmpty(htmlCode) ? " " : htmlCode);
                    }
                    else
                    {
                        var htmlCode = " ";
                        webSocketServiceAgent.SendString(htmlCode);
                    }
                    IsToggled_NextDataIsHtmlCode = false;

                    DataText_Name = serviceAgentElastic.OItemHtmlDocument != null ?  serviceAgentElastic.OItemHtmlDocument.Name : referenceItem.Name;
                }
                else if (webSocketServiceAgent.Command_RequestedCommand == Notifications.NotifyChanges.Command_ReceiveContent)
                {
                    webSocketServiceAgent.InterpretNextAsBytes = true;
                }
                else if (webSocketServiceAgent.Command_RequestedCommand.ToLower().StartsWith("clicked.isListen".ToLower()))
                {
                    IsToggled_Listen = !IsToggled_Listen;
                }
                else if (webSocketServiceAgent.Command_RequestedCommand == "clickedNoInsert")
                {
                    IsToggled_InsertLink = false;
                    IsToggled_InsertAnchor = false;   
                }
                else if (webSocketServiceAgent.Command_RequestedCommand == "clickedToggleLink")
                {
                    if (IsToggled_InsertAnchor || (!IsToggled_InsertAnchor && !IsToggled_InsertLink))
                    {
                        IsToggled_InsertLink = true;
                        IsToggled_InsertAnchor = false;
                    }
                }
                else if (webSocketServiceAgent.Command_RequestedCommand == "clickedToggleAnchor")
                {
                    if (IsToggled_InsertLink || (!IsToggled_InsertAnchor && !IsToggled_InsertLink))
                    {
                        IsToggled_InsertLink = false;
                        IsToggled_InsertAnchor = true;
                    }
                }
                else if (webSocketServiceAgent.Command_RequestedCommand == "clicked.sendSelect")
                {
                    if (serviceAgentElastic.OItemHtmlDocument == null) return;

                    var interServiceMessage = new InterServiceMessage
                    {
                        ChannelId = Channels.ParameterList,
                        OItems = new List<clsOntologyItem>
                        {
                            serviceAgentElastic.OItemHtmlDocument
                        }
                    };
                    webSocketServiceAgent.SendInterModMessage(interServiceMessage);
                }
                else if (webSocketServiceAgent.Command_RequestedCommand == "clicked.sendApply")
                {
                    if (serviceAgentElastic.OItemHtmlDocument == null) return;

                    var interServiceMessage = new InterServiceMessage
                    {
                        ChannelId = Channels.AppliedObjects,
                        OItems = new List<clsOntologyItem>
                        {
                            serviceAgentElastic.OItemHtmlDocument
                        }
                    };
                    webSocketServiceAgent.SendInterModMessage(interServiceMessage);
                }
                

            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_Event_RequestEvent)
            {

            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_ChangedProperty)
            {
                if (webSocketServiceAgent.ChangedProperty.Key == Notifications.NotifyChanges.ViewModel_DataText_Name)
                {
                    var value = webSocketServiceAgent.ChangedProperty.Value;
                    if (value == null)
                    {
                        DataText_Name = "";
                    }
                    else
                    {
                        DataText_Name = webSocketServiceAgent.ChangedProperty.Value.ToString();
                    }

                    var result = serviceAgentElastic.SaveDocName(DataText_Name);
                    if (result.GUID == localConfig.Globals.LState_Error.GUID)
                    {
                        DataText_Name = serviceAgentElastic.OItemHtmlDocument != null ? serviceAgentElastic.OItemHtmlDocument.Name : "";
                    }
                }
            }
            else if (e.PropertyName == NotifyChanges.Websocket_ObjectArgument)
            {
                var idObject = webSocketServiceAgent.ObjectArgument.Value;

                if (!localConfig.Globals.is_GUID(idObject)) return;

                var oItem = serviceAgentElastic.GetOItem(idObject, localConfig.Globals.Type_Object);

                if (oItem == null) return;

                referenceItem = oItem;

                SelectedItem();
            }
            else if (e.PropertyName == NotifyChanges.Websocket_ChangedViewItems)
            {
                webSocketServiceAgent.ChangedViewItems.ForEach(viewItem =>
                {
                    if (viewItem.ViewItemId == "selectedItem")
                    {
                        var id = viewItem.ViewItemValue.ToString();

                        if (string.IsNullOrEmpty(id)) return;

                        id = id.Replace("_", "");

                        if (!localConfig.Globals.is_GUID(id)) return;

                        var item = serviceAgentElastic.GetOItem(id, localConfig.Globals.Type_Object);

                        if (item == null || item.GUID == localConfig.Globals.LState_Error.GUID) return;

                        var typedTag = typedTaggingConnector.ResultFoundTypedTags.TypedTags.FirstOrDefault(typedTagItm => typedTagItm.IdTypedTag == item.GUID);

                        if (typedTag != null)
                        {
                            item = serviceAgentElastic.GetOItem(typedTag.IdTag, localConfig.Globals.Type_Object);
                        }

                        if (item == null || item.GUID == localConfig.Globals.LState_Error.GUID) return;

                        var interserviceMessage = new InterServiceMessage
                        {
                            ChannelId = Channels.ParameterList,
                            OItems = new List<clsOntologyItem>
                            {
                                item        
                            }
                        };

                        webSocketServiceAgent.SendInterModMessage(interserviceMessage);
                    }
                    

                });
            }


        }

        private void WebSocketServiceAgent_comServerOpened()
        {
            var authenticationRequest = new InterServiceMessage
            {
                ChannelId = Channels.Login,
                SenderId = webSocketServiceAgent.EndpointId
            };

            webSocketServiceAgent.SendInterModMessage(authenticationRequest);

        }

        private void WebSocketServiceAgent_comServerOnMessage(OntoMsg_Module.Notifications.InterServiceMessage message)
        {
            if (!IsSuccessful_Login) return;
            if (message.ReceiverId != null && message.ReceiverId != webSocketServiceAgent.EndpointId) return;
            if (message.ChannelId == Channels.ParameterList && IsToggled_Listen)
            {
                var objectItem = message.OItems.LastOrDefault();

                if (objectItem == null) return;

                if (referenceItem != null && referenceItem.GUID == objectItem.GUID) return;

                referenceItem = objectItem;

                SelectedItem();
            }
            else if (message.ChannelId == Channels.AppliedObjects && serviceAgentElastic.OItemHtmlDocument != null && (IsToggled_InsertAnchor || IsToggled_InsertLink))
            {
                message.OItems.ForEach(oItem =>
                {
                    oItem.Val_Long = 1;
                });
                var resultTask = typedTaggingConnector.SaveTags(serviceAgentElastic.OItemHtmlDocument, message.OItems, webSocketServiceAgent.oItemUser, webSocketServiceAgent.oItemGroup, true);

                
            }
            
        }

        private void SelectedItem()
        {
            WebSocketBase.GetAnonymousItems();
            var show = (WebSocketBase.AnonymousItems.Any(anonym => anonym.GUID == referenceItem.GUID) || IsSuccessful_Login);

            if (!show) return;
            referenceParentItem = serviceAgentElastic.GetOItem(referenceItem.GUID_Parent, localConfig.Globals.Type_Class);

            Text_View = referenceItem.Name + " (" + referenceParentItem.Name + ")";

            var result = serviceAgentElastic.GetRelatedHtmlDocument(referenceItem);

            IsToggled_Listen = false;
        }

        public List<ViewModelProperty> GetViewModelProperties(bool onlySend = true, ViewItemType viewItemType = ViewItemType.All, ViewItemClass viewItemClass = ViewItemClass.All)
        {
            return ViewModelProperties.Where(viewItemProp => (onlySend ? viewItemProp.ViewModelAttribute.Send : 1 == 1)
                && (viewItemType != ViewItemType.All ? viewItemProp.ViewModelAttribute.ViewItemType == viewItemType : 1 == 1)
                && (viewItemClass != ViewItemClass.All ? viewItemProp.ViewModelAttribute.ViewItemClass == viewItemClass : 1 == 1)).ToList();
        }
    }
}
