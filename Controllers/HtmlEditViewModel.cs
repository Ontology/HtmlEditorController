﻿using HtmlEditorController.Notifications;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HtmlEditorController.Controllers
{
    public class HtmlEditViewModel : OntoMsg_Module.Base.ViewModelBase
    {
        private bool issuccessful_Login;
        [ViewModel(Send = true)]
        public bool IsSuccessful_Login
        {
            get { return issuccessful_Login; }
            set
            {

                issuccessful_Login = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsSuccessful_Login);

            }
        }

        private bool isToggled_Listen;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "isListen", ViewItemType = ViewItemType.Other)]
        public bool IsToggled_Listen
        {
            get { return isToggled_Listen; }
            set
            {
                if (isToggled_Listen == value) return;

                isToggled_Listen = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsToggled_Listen);

            }
        }

        private bool istoggled_NextDataIsHtmlCode;
        [ViewModel(Send = true)]
        public bool IsToggled_NextDataIsHtmlCode
        {
            get { return istoggled_NextDataIsHtmlCode; }
            set
            {

                istoggled_NextDataIsHtmlCode = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsToggled_NextDataIsHtmlCode);

            }
        }

        private string datatext_Name;
        [ViewModel(Send = true)]
        public string DataText_Name
        {
            get { return datatext_Name; }
            set
            {
                if (datatext_Name == value) return;

                datatext_Name = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_DataText_Name);

            }
        }

        private string label_Name;
        [ViewModel(Send = true)]
        public string Label_Name
        {
            get { return label_Name; }
            set
            {
                if (label_Name == value) return;

                label_Name = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Label_Name);

            }
        }

        private bool isenabled_HtmlEditor;
        [ViewModel(Send = true)]
        public bool IsEnabled_HtmlEditor
        {
            get { return isenabled_HtmlEditor; }
            set
            {
                if (isenabled_HtmlEditor == value) return;

                isenabled_HtmlEditor = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_HtmlEditor);

            }
        }


        private string datatext_ViewId;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "viewId", ViewItemType = ViewItemType.Other)]
        public string DataText_ViewId
        {
            get { return datatext_ViewId; }
            set
            {
                if (datatext_ViewId == value) return;

                datatext_ViewId = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_DataText_ViewId);

            }
        }

        private string datatext_AttributeTypeId;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "attributeTypeId", ViewItemType = ViewItemType.Other)]
        public string DataText_AttributeTypeId
        {
            get { return datatext_AttributeTypeId; }
            set
            {
                if (datatext_AttributeTypeId == value) return;

                datatext_AttributeTypeId = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_DataText_AttributeTypeId);

            }
        }

        private string datatext_RelationTypeId;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "relationTypeId", ViewItemType = ViewItemType.Other)]
        public string DataText_RelationTypeId
        {
            get { return datatext_RelationTypeId; }
            set
            {
                if (datatext_RelationTypeId == value) return;

                datatext_RelationTypeId = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_DataText_RelationTypeId);

            }
        }

        private string datatext_ObjectId;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "objectId", ViewItemType = ViewItemType.Other)]
        public string DataText_ObjectId
        {
            get { return datatext_ObjectId; }
            set
            {
                if (datatext_ObjectId == value) return;

                datatext_ObjectId = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_DataText_ObjectId);

            }
        }

        private string datatext_ClassId;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "classId", ViewItemType = ViewItemType.Other)]
        public string DataText_ClassId
        {
            get { return datatext_ClassId; }
            set
            {
                if (datatext_ClassId == value) return;

                datatext_ClassId = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_DataText_ClassId);

            }
        }

        private string text_View;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "viewText", ViewItemType = ViewItemType.Other)]
		public string Text_View
        {
            get { return text_View; }
            set
            {
                if (text_View == value) return;

                text_View = value;

                RaisePropertyChanged(NotifyChanges.View_Text_View);

            }
        }

        private string text_Selection;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Selection, ViewItemId = "selectedText", ViewItemType = ViewItemType.Content)]
		public string Text_Selection
        {
            get { return text_Selection; }
            set
            {
                if (text_Selection == value) return;

                text_Selection = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Text_Selection);

            }
        }

        private string text_Insert;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "textInsert", ViewItemType = ViewItemType.Content)]
		public string Text_Insert
        {
            get { return text_Insert; }
            set
            {

                text_Insert = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Text_Insert);

            }
        }

        private bool istoggled_InsertAnchor;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.ToggleButton, ViewItemId = "tglInsertAnchor", ViewItemType = ViewItemType.Checked)]
		public bool IsToggled_InsertAnchor
        {
            get { return istoggled_InsertAnchor; }
            set
            {
                if (istoggled_InsertAnchor == value) return;

                istoggled_InsertAnchor = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsToggled_InsertAnchor);

            }
        }

        private bool istoggled_InsertLink = true;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.ToggleButton, ViewItemId = "tglInsertLink", ViewItemType = ViewItemType.Checked)]
		public bool IsToggled_InsertLink
        {
            get { return istoggled_InsertLink; }
            set
            {

                istoggled_InsertLink = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsToggled_InsertLink);

            }
        }

        private string datatext_Anchor;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "idAnchor", ViewItemType = ViewItemType.Other)]
		public string DataText_Anchor
        {
            get { return datatext_Anchor; }
            set
            {
                datatext_Anchor = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_DataText_Anchor);

            }
        }

        private bool isenabled_ShowViewerArea;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "showViewerArea", ViewItemType = ViewItemType.Enable)]
		public bool IsEnabled_ShowViewerArea
        {
            get { return isenabled_ShowViewerArea; }
            set
            {

                isenabled_ShowViewerArea = value;

                RaisePropertyChanged(nameof(IsEnabled_ShowViewerArea));

            }
        }
    }
}
