﻿using OntologyAppDBConnector.Base;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace HtmlEditorController.Controllers
{
    public class HtmlAnaylzer : NotifyPropertyChange
    {
        private clsLocalConfig localConfig;

        private GuidListResult guidListResult;
        public GuidListResult GuidListResult
        {
            get { return guidListResult; }
            set
            {
                guidListResult = value;
                RaisePropertyChanged(nameof(GuidListResult));
            }
        }

        public async Task<GuidListResult> GetGuidList(clsObjectAtt oAttributeHtml)
        {
            var regexGuid = new Regex("name=\"_?" + localConfig.Globals.RegexGuid + "\"");
            var foundGuids = regexGuid.Matches(oAttributeHtml.Val_String);
            var result = new GuidListResult
            {
                Result = localConfig.Globals.LState_Success.Clone(),
                GuidList = foundGuids.Cast<Match>().Select(match => match.Value.Replace("_", "").Replace("name=", "").Replace("\"", "")).Where(matchValue => localConfig.Globals.is_GUID(matchValue)).GroupBy(guid => new { guid }).Select(guidItem => guidItem.Key.guid).ToList()
            };
            GuidListResult = result;
            return GuidListResult;
        }

        public HtmlAnaylzer(clsLocalConfig localConfig)
        {
            this.localConfig = localConfig;
        }
    }

    public class GuidListResult
    {
        public clsOntologyItem Result { get; set; }
        public List<string> GuidList { get; set; }
    }
}
